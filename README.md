vim-simple
==========

This is a minimal `$VIMRC` file, containing only options and mappings with no external dependencies, available via `curl -L bit.ly/vimrcmin -o .vimrc` or via `wget bit.ly/vimrcmin -O .vimrc`

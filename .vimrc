" vim: set nowrap:

" HEADER
" Description: Vim config file (compatible with defaults.vim)
" Author: dsjkvf@gmail.com
" Webpage: https://bit.ly/vimrcmin
" Webpage: https://bitbucket.org/dsjkvf/achilles
" Notes: for test purposes only


" ENVIRONMENT

" Set <Leader>
let mapleader = ','

" Set $VIMRC and $VIMHOME
let $VIMRC = expand('~/.vimrc')
let $VIMHOME = expand('~/.vim')


" PLUGINS

" Enable stock plugins
" matchit
if has("patch-7.4.1649")
    packadd! matchit
endif
" cfilter
if has("patch-8.1.0360")
    packadd! cfilter
endif

" Configure plugins
" matchit
let g:matchparen_timeout = 20
let g:matchparen_insert_timeout = 20
nnoremap <Leader>M :call plugins#matchit#SetMatchItPair()<CR>
nnoremap <silent> <Tab> :normal %<CR>
xnoremap <silent> <Tab> :normal %<CR>m`gv``
" netrw
let g:netrw_liststyle = 0
let g:netrw_sort_sequence = '^$,[\/]$'
let g:netrw_banner = 0
let g:netrw_browse_split = 3
let g:netrw_alto = '&sb'
let g:netrw_silent = 1
command! ML echo join(netrw#Expose("netrwmarkfilelist"), "\n")


" INTERFACE

" Backgrounds, colorschemes
set background=dark
nnoremap <silent> <expr> <Leader>col :&colorcolumn == '' ? ':let &colorcolumn=join(range(81,999),",")<CR>' : ':let &colorcolumn=<CR>'
if globpath(&runtimepath, 'colors/silence.vim') != ''
   colorscheme silence
endif
nnoremap <Leader>cls :colorscheme 

" Cursors
" let &t_SI = "\<Esc>[6 q"
" let &t_SR = "\<Esc>[4 q"
" let &t_EI = "\<Esc>[2 q"

" Windows
" position new windows
set splitbelow
set splitright
" open vertical or horizontal splits
nnoremap <silent> <expr> \| !v:count ? "<C-W>v<C-W><Right>" : '\|'
nnoremap <silent> <expr> _  !v:count ? "<C-W>s<C-W><Down>"  : '_'
" bind two splits to scroll alongside
command! BindSplitsOn set scrollbind cursorbind | wincmd p | set scrollbind cursorbind | wincmd p
command! BindSplitsOff set noscrollbind nocursorbind | wincmd p | set noscrollbind nocursorbind | wincmd p
" set the minimal height of a window, when it's not the current window
set winminheight=0
set winminwidth=0
" autoresize windows on Vim's window's size change
autocmd! VimResized * wincmd =

" Syntax
syntax off
nnoremap <expr> cs exists("g:syntax_on") ? ':syntax off<CR>' : ':syntax enable<CR>'

" Statusline
function! IsModOrNot()
    highlight IsMod cterm=NONE ctermbg=088 ctermfg=NONE gui=NONE guibg=#800000 guifg=NONE
    if &mod == 1 && &buftype != 'terminal'
        setlocal statusline=%#IsMod#[%l/%L,\ %v]\ [%p%%]\ [TYPE=%Y]\ [FMT=%{&ff}]\ %{\"[ENC=\".(&fenc==\"\"?&enc:&fenc).\"]\"}\ %{&bomb?'[BOM]':''}\ %=%F%m%r%h%w\ %{IsModOrNot()}\[%n]%<
    else
        setlocal statusline=[%l/%L,\ %v]\ [%p%%]\ [TYPE=%Y]\ [FMT=%{&ff}]\ %{\"[ENC=\".(&fenc==\"\"?&enc:&fenc).\"]\"}\ %{&bomb?'[BOM]':''}\ %=%F%m%r%h%w\ %{IsModOrNot()}\[%n]%<
    endif
    return ''
endfunction
set laststatus=2
set statusline=
set statusline+=%{IsModOrNot()}

" Numberline
set number
set numberwidth=4

" Menus
" disable GUI menus (if compiled with GUI)
" set guioptions=M
" command line completion related
" set wildmode=longest,list "default is full
set wildmenu
" character you have to type to start wildcard expansion
set wildcharm=<C-z>
" define wildignore
" exclude parent folders
set wildignore=*../
" binaries, libraries
set wildignore+=*.a,*.o,*.obj,*.exe,*.dll,*.so,*.manifes,*.luac,*.pyc,*.pyo,*.pdf,*.spl
" r objects
set wildignore+=*.RData,*.Rdata,*.RDS,*.rds
" archives
set wildignore+=*.zip,*.rar,*.tar,*.tar.gz,*.tar.bz,*.tar.bz2
" documents
set wildignore+=*.jpg,*.bmp,*.gif,*.png,*.jpeg,*.ico,*.pdf,*.docx,*.xls,*.xlsx
" intermediate, temp and garbage files
set wildignore+=*.aux,*.out,*.toc,*.bak,*.sw?,*.DS_Store,%*
if has('win32') || has ('win64')
    set wildignore+=*\\.git\\*,*\\.hg\\*,*\\.svn\\*,*\\bin\\*,*\\pkg\\*,*\\vendor\\*
else
    set wildignore+=.git/,*/.git/,*/.git/*,*/.hg/*,*/.svn/*,*/bin/*,*/pkg/*,*/vendor/*
endif
" and make a global copy of those
let g:wildignore_orig = &wildignore
" display commands
set showcmd
" ask a confirmation
set confirm
" keep 10000 lines of command line history
set history=10000

" Crosshair
nnoremap <silent> <Leader>= :set cursorline!<CR>
nnoremap <silent> <Leader>+ :set cursorcolumn!<CR>

" Scrolling
" do not display '@' symbols if the last line doesn't fit
set display+=lastline
" use Space as precise PgDn
nnoremap <expr> <Space> line('w$') >= line('$') ? 'L' : 'z+'
" and <S-Space> as precise PgUp
" xnoremap <Space> z+
" (iTerm sends the HEX value of C-b, 0x02, for S-Space thus the latter already acts as PgUp, and therefore we remap <C-b> here)
nnoremap <C-b> z^
xnoremap <C-b> z^
" scroll, cursor moves with scroll
nnoremap <C-u> <C-e>
nnoremap <C-y> <C-y>
" scroll, cursor stays intact
nnoremap <C-j> ^<C-e>j
nnoremap <C-k> ^<C-y>k
" make zt/zb work nicely with visual selections
xnoremap <silent> zt :<c-u>call setpos('.',[0,line("'<"),0,0])<CR>:normal! ztgv<CR>
xnoremap <silent> zb :<c-u>call setpos('.',[0,line("'>"),0,0])<CR>:normal! zbgv<CR>
" make H/L scroll as well
nnoremap <silent> <expr> L line('.') < line('w$') ? 'L' : 'z+L'
nnoremap <silent> <expr> H line('.') > line('w0') ? 'H' : 'z^H'
" swap zl and zh
nnoremap zh zl
nnoremap zl zh
" swap zL and zH
nnoremap zH zL
nnoremap zL zH

" Wrapping
" disable wrapping by default
set nowrap
nnoremap ∑ :setlocal wrap!<CR>:set wrap?<CR>

" Misc
set fillchars=vert:│,fold:—,diff:—
set listchars=tab:»\ ,trail:·
set list
" set lazyredraw
set ttyfast
set ttyscroll=3

" Error handling
" no error bells
set vb t_vb=


" NAVIGATION

" Keyboard
" https://stackoverflow.com/questions/2158516/delay-before-o-opens-a-new-line
set timeout timeoutlen=3000 ttimeoutlen=100

" Mouse
set mouse=a
if exists('&ttymouse')
    set ttymouse=xterm2
endif

" Misc
" try to stay in the same column after executing a command
set nostartofline
" allow 'h' and 'l' to move you to previous/next line when reached
set whichwrap+=>,l
set whichwrap+=<,h
" repeat latest f, t, F or T in opposite direction
" (since the default mapping, comma, is set to Leader key)
nnoremap <silent> <Leader>; ,
" select a line without leading spaces and a line break
" nnoremap <silent> ≠ ^v$h
" go to the beginning of the line and switch to insert mode
nnoremap <silent> <C-a> I
inoremap <C-a> <Esc>I
" go to the end of the line and switch to insert mode
nnoremap <silent> <C-e> A
inoremap <C-e> <Esc>A
" go to the beginning of the line
nnoremap ≠ ^
nnoremap ± ^
" go to the previous word
" option-left
set <F27>=[1;3D
map <F27> <M-Left>
imap <F27> <M-Left>
cmap <F27> <M-Left>
if v:version >= 800
    tmap <F27> <M-Left>
endif
nnoremap <M-Left> <S-Left>
xnoremap <M-Left> <S-Left>
inoremap <M-Left> <S-Left>
" ctrl-left
set <F31>=[1;5D
map <F31> <C-Left>
imap <F31> <C-Left>
nnoremap <C-Left> <S-Left>
xnoremap <C-Left> <S-Left>
inoremap <C-Left> <S-Left>
" go to the next word
" option-right
set <F28>=[1;3C
map <F28> <M-Right>
imap <F28> <M-Right>
cmap <F28> <M-Right>
if v:version >= 800
    tmap <F28> <M-Right>
endif
nnoremap <M-Right> <S-Right>
xnoremap <M-Right> <S-Right>
inoremap <M-Right> <S-Right>
" ctrl-right
set <F32>=[1;5C
map <F32> <C-Right>
imap <F32> <C-Right>
nnoremap <C-Right> <S-Right>
xnoremap <C-Right> <S-Right>
inoremap <C-Right> <S-Right>
" bookmark the current line before going to the top of the file
nnoremap gg gg''mz''
" bookmark the current line before going to the bottom of the file (or any other line)
nnoremap G G''mz''zzzv
" navigate the command line
" go to the beginning of the line
cnoremap <C-a> <Home>
" go to the end of the line
cnoremap <C-e> <End>
" go to the previous word
cnoremap <M-left> <S-Left>
" go to the next word
cnoremap <M-Right> <S-Right>
" comment the current command (AltGr-k)
cnoremap ˚ <Home>"
" navigate with changeiist
" go to [count] changes backward
nnoremap g' g,
" go to [count] changes forward
" navigate with jumplist
" nnoremap g; g;
" go to [count] jumps backward
nnoremap g, <C-O>
nnoremap =[ <C-O>
" go to [count] jumps forward
nnoremap g. <C-I>
nnoremap =] <C-I>
" moving through lines
" allow <Down> / <Up> to move through wrapped lines
nnoremap <silent> <Down> gj
inoremap <silent> <Down> <C-o>gj
nnoremap <silent> <Up> gk
inoremap <silent> <Up> <C-o>gk
" allow j / k to move through wrapped lines
" also, allow jumps like 3j / 7k to be added to the jumplist, subsequently allowing g, / g.
nnoremap <expr> j v:count ? "m'" . v:count . 'j' : 'gj'
nnoremap <expr> k v:count ? "m'" . v:count . 'k' : 'gk'
" moving lines
set <F29>=[1;5A
map <F29> <C-Up>
imap <F29> <C-Up>
nnoremap <silent> <C-Up> :<C-u>move-2<CR>==
xnoremap <silent> <C-Up> :move-2<CR>gv=gv
inoremap <silent> <C-Up> <C-o>:<C-u>move-2<CR><C-o>==
set <F30>=[1;5B
map <F30> <C-Down>
imap <F30> <C-Down>
nnoremap <silent> <C-Down> :<C-u>move+<CR>==
xnoremap <silent> <C-Down> :move'>+<CR>gv=gv
inoremap <silent> <C-Down> <C-o>:<C-u>move+<CR><C-o>==


" EDITING

" Misc
" enable file type detection, load indent files
filetype plugin indent on
" remove a comment leader when joining lines
if has("patch-7.4.550")
    set formatoptions +=j
endif
" never insert two spaces after '.', '!', or '?'
set nojoinspaces
" make dash a part of a word
set iskeyword+=-
" backspace over everything in insert mode.
set backspace=indent,eol,start

" History
" keep 1000 lines of command line history
set history=1000
" keep 40 latest files
set viminfo='40,<40,s10,h

" Search
set hlsearch
" do incremental searching
set incsearch
" ignore case
set ignorecase
" but don't ignore it, when search string contains uppercase letters
set smartcase
" show matching parentheses
set showmatch
" show search count
set shortmess-=S
" reset last search pattern
nnoremap <silent> <BS> :echo ""<CR>:nohlsearch<CR>
" search and replace
nnoremap <Leader>h :%s/
" search and replace the word under cursor
nnoremap <Leader>H :%s/<C-r><C-w>/
" center search match after pressing <CR>
cnoremap <expr> <CR> getcmdtype() == '/' <bar><bar> getcmdtype() == '?' ? '<CR>zz' : '<CR>'

" Indenting
" use spaces instead of tabs
set expandtab
" number of spaces for a tab character
set tabstop=4
" number of spaces for inserting a tab or a backspace character
set softtabstop=4
" inserts blanks according to 'tabstop' or 'softtabstop' (this is the default setting)
" set nosmarttab
" inserts blanks according to 'shiftwidth'
" set smarttab
set autoindent
set copyindent
" number of spaces for the indent command
set shiftwidth=4
" round indent command to multiple of 'shiftwidth'
set shiftround
" wrap long lines at a character in 'breakat'
" https://www.reddit.com/3rlop3/
if has('linebreak')
    set linebreak
    let &showbreak = '↳ '
"     set cpoptions+=n
    if v:version > 703
        " continue wrapped lines visually indented
        set breakindent
    endif
endif
" html specific settings
" https://www.reddit.com/97e33c/
" https://www.reddit.com/bxu87z/
let g:html_indent_script1 = 'inc'
let g:html_indent_style1  = 'inc'
let g:html_indent_inctags = 'html,body,head,tbody,p,li,dd,dt,h1,h2,h3,h4,h5,h6,blockquote,section,script,style'
" change indentation without leaving the visual selection
xnoremap < <gv
xnoremap > >gv

" Files and directories
" exclude /usr/include from path
set path-=/usr/include
" set directories for swap, backup and undo files
if !isdirectory($VIMHOME . '/tmp')
    call mkdir($VIMHOME . '/tmp', 'p')
endif
set undodir=$VIMHOME/tmp//
set backupdir=$VIMHOME/tmp//
set directory=$VIMHOME/tmp//
set undofile
" change the current working directory according to an opened file
" set autochdir
augroup AutoChangeDirs
    autocmd!
    autocmd BufEnter * silent! lcd %:p:h
augroup END

" Copying / Pasting
" fix bracketed paste for TMUX / GNU Screen (:h xterm-bracketed-paste)
if &term =~ "screen" || &term =~ "tmux"
    let &t_BE = "\e[?2004h"
    let &t_BD = "\e[?2004l"
    exec "set t_PS=\e[200~"
    exec "set t_PE=\e[201~"
endif
" delete to the end of line
nnoremap <silent> D d$
" yank to the end of line
nnoremap <silent> Y y$
" yank with Enter (except for quickfix, see below)
nnoremap <CR> y
xnoremap <CR> y`]
" yank and jump to the end of the yanked text
nnoremap <silent> yy yy`]
xnoremap <silent> y y`]
" paste and jump to the end of the pasted text
nnoremap <silent> p p`]
nnoremap <silent> P P`]
" paste over a selection, keep the unnamed register untouched and jump to the end of the pasted text
xnoremap <expr> p 'pgv"' . v:register . 'y`]'
" paste over a selection, update the unnamed register and jump to the end of the pasted text
xnoremap <silent> P p`]
" paste from the yanks register, keep the unnamed register untouched and jump to the end of the pasted text
nnoremap <silent> <Leader>y "0p`]
nnoremap <silent> <Leader>Y "0P`]
xnoremap <silent> <Leader>y "_d"0p`]
xnoremap <silent> <Leader>Y "_d"0P`]
" paste from the system clipboard, keep the unnamed register untouched and jump to the end of the pasted text
nnoremap <silent> <Leader>p :let @+ = trim(@+)<CR>"+p`]
nnoremap <silent> <Leader>P :let @+ = trim(@+)<CR>+P`]
xnoremap <silent> <Leader>p :let @+ = trim(@+)<CR>+p`]
xnoremap <silent> <Leader>P :let @+ = trim(@+)<CR>+P`]
" reselect the pasted text
nnoremap <expr> <Leader>gv "`[" . visualmode() . "`]"
" delete to the unnamed register and start editing
nnoremap x "_s
xnoremap x "_c
" delete a word in insert mode (cut)
inoremap <C-d> <C-o>:call feedkeys("\<C-O>dw")<CR>
" delete a word backwards in insert mode, insert an undo point
inoremap <C-w> <C-g>u<C-w>
" delete a word while in the command line (delete, not cut)
cnoremap <expr> <C-d> getcmdpos() != len(getcmdline()) + 1 ? '<S-Right><Right><C-w>' : '<C-d>'
" clear after the cursor while in the command line (delete, not cut)
cnoremap <C-k> <C-\>e getcmdpos() == 1 ? '' : getcmdline()[:getcmdpos()-2]<CR>

" Commenting
" toggle comments
nnoremap <silent> <expr> <Leader>m getline('.')[0] == &commentstring[0]
            \ ? ':s/.*/\=submatch(0)[2:]/<CR>:nohl<CR>'
            \ : ':s/.*/\=&commentstring[0:1] . submatch(0)/<CR>:nohl<CR>'
xnoremap <silent> <expr> <Leader>m getline('.')[0] == &commentstring[0]
            \ ? ':s/.*/\=submatch(0)[2:]/<CR>:nohl<CR>'
            \ : ':s/.*/\=&commentstring[0:1] . submatch(0)/<CR>:nohl<CR>'

" Surrounding
" in visual mode
xnoremap s{ <Esc>`<i{<Esc>`>la}<Esc>
xnoremap s( <Esc>`<i(<Esc>`>la)<Esc>
xnoremap s[ <Esc>`<i[<Esc>`>la]<Esc>
xnoremap s' <Esc>`<i'<Esc>`>la'<Esc>
xnoremap s" <Esc>`<i"<Esc>`>la"<Esc>
xnoremap s< <Esc>`<i<<Esc>`>la><Esc>
" in normal mode
function! Surroundings()
    let o1 = getchar() | let o1 = type(o1) == type(0) ? nr2char(o1) : o1
    let o2 = getchar() | let o2 = type(o2) == type(0) ? nr2char(o2) : o2
    let o_ = getchar() | let o_ = type(o_) == type(0) ? nr2char(o_) : o_
    execute "normal v" . o1 . o2 . "s" . o_
endfunction
nnoremap <silent> s :call Surroundings()<CR>

" Targeting
" next (
onoremap in( :<C-u>normal! f(vi(<CR>
" last (
onoremap il( :<C-u>normal! F)vi(<CR>
" next {
onoremap in{ :<C-u>normal! f{vi{<CR>
" last {
onoremap il{ :<C-u>normal! F}vi{<CR>
" next [
onoremap in[ :<C-u>normal! f[vi[<CR>
" last [
onoremap il[ :<C-u>normal! F]vi[<CR>
" next "
onoremap in" :<C-u>normal! f"vi"<CR>
" last "
onoremap il" :<C-u>normal! F"vi"<CR>
" next '
onoremap in' :<C-u>normal! f'vi'<CR>
" last '
onoremap il' :<C-u>normal! F'vi'<CR>
" next <
onoremap in' :<C-u>normal! f<vi'<CR>
" last <
onoremap il' :<C-u>normal! F>vi'<CR>

" Codepages
if has("multi_byte")
    " set the encoding used inside Vim (sets the default fileencoding as well)
    set encoding=utf-8 nobomb
    if &termencoding == ""
        let &termencoding = &encoding
    endif
    " set the encodings and format to try when opening a new file
    set fileencodings=utf-8,cp1251,koi8-r,latin1
    set fileformats=unix,dos,mac
endif

" Open
" open any file (see below for a few cnoremaps)
nnoremap <Leader>e :tabedit 
" open any file, start from the $HOME folder
nnoremap <Leader>E :tabedit ~/
" add masks to any path
cnoremap <C-s> **/*
cnoremap ß **/.[^\/]
" add hardmapped directories
" the current directory
cnoremap ~~ <C-R>=fnameescape(expand('%:p:h'))<cr>/
" some others, too
cnoremap ~d ~/Desktop/
cnoremap ~o ~/Documents/
cnoremap ~v $VIMHOME/

" Save and quit
nnoremap <F10> :q<CR>
if $SSH_CONNECTION != ''
    set <F13>=[1;2P
else
    set <F13>=[25~
endif
nnoremap <expr> <F13> &modified ? ':w<CR>' : ':q<CR>'
inoremap <F13> <Esc>:w<CR>
inoremap <F1> <Esc>:w<CR>

" Save as
set <S-F13>=[1;2Q
nnoremap <S-F13> :sav <C-R>=fnameescape(expand('%'))<CR>

" Reload
nnoremap <Leader>r :w<CR>:e<CR>

" Undo and redo
nnoremap <silent> u :undo<CR>zv
nnoremap <silent> U :redo<CR>zv

" Edit
" dot-repeat on visually selected lines (edit, <Esc>, select, .)
xnoremap <silent> normal! .<CR>
" uppercase the current character
set <F33>=[1;2A
map <F33> <S-Up>
nnoremap <S-Up> gUl
xnoremap <S-Up> gU
" lowercase the current character
set <F34>=[1;2B
map <F34> <S-Down>
nnoremap <S-Down> gul
xnoremap <S-Down> gu
" increase or decrease an alphabetic character at or after the cursor
if has("patch-7.4.1027")
    set nrformats=alpha,bin,hex
endif
nnoremap ga <C-a>
xnoremap ga <C-a>
nnoremap gs <C-x>
xnoremap gs <C-x>
xnoremap gA g<C-a>
xnoremap gS g<C-x>

" Quickfix
" settings
augroup ManagingQuickfix
    autocmd!
    if v:version > 704
        autocmd WinNew * call setloclist(win_getid(winnr()), [])
    endif
    " open on errors, close on no errors
    autocmd QuickFixCmdPost [^l]* cwindow
    autocmd QuickFixCmdPost    l* lwindow
augroup END

" Command window
" settings
augroup CommandWindow
    autocmd!
    " disable numberline
    autocmd CmdwinEnter * set nonumber
    " make Enter continue to act as y
    autocmd CmdwinEnter * onoremap <buffer> <CR> y
    " obsolete:
    " start in insert mode
    " autocmd CmdwinEnter * startinsert
    " make Enter act as Enter
    " autocmd CmdwinEnter * nnoremap <buffer> <CR> <CR>
    " autocmd CmdwinEnter * xnoremap <buffer> <CR> :y v<CR>:q<CR>:execute substitute(@v, '\n\s*\\ ', ' ', 'g')<CR>
augroup END

" Macros (qq to record, q to stop, Q to run; crq to edit (or use <Ctrl-r><Ctrl-r>q))
" run a macro
nnoremap Q @q
" run a macro on visually selected lines
xnoremap Q :normal @q<CR>

" Remove empty lines
command! -range=% RemoveLinesEmpty <line1>,<line2>s/^\s*\n// | nohl | normal ``

" Compress empty lines
command! -range=% CompressLinesEmpty <line1>,<line2>v/./,/./-j | nohl | normal ``

" Remove ^M chars
command! -range=% RemoveLinesM <line1>,<line2>s///ge | nohl | normal ``

" Remove leading spaces
command! -range=% RemoveSpacesLeading <line1>,<line2>s/^ *//e | nohl | normal ``

" Remove trailing spaces
command! -range=% RemoveSpacesTrailing <line1>,<line2>s/\s\+$//e | nohl | normal ``

" Remove multiple spaces
command! -range=% RemoveSpacesMultiple <line1>,<line2>s/\s\+/ /ge | nohl | normal ``

" Replace tabs with spaces
command! -range=% ReplaceTabsWithSpaces <line1>,<line2>s/\t/    /ge | nohl | normal ``

" Replace "2 x space" tabs with "4 x space" tabs
command! -range=% Replace2spacetabsWith4spacetabs set ts=2 sts=2 noet | retab! | set ts=4 sts=4 et | retab

" Remove HTML formatting
command! -range=% RemoveHTMLtags <line1>,<line2>s/<\_.\{-1,\}>//g

" Remove "hard wrapping" (replace with "soft wrapping")
command! -range=% RemoveHardWrap <line1>,<line2>g/^\s*\n.*\S$/+norm vipJ

" Shell
nnoremap <silent> <expr> <C-d> getcwd() != expand('%:p:h')
            \ ? ':execute "cd " . resolve(expand("%:p:h"))<Bar>sh<CR>'
            \ : ':sh<CR>'

" Scratch
" by /u/borring
" https://www.reddit.com/r/vim/comments/3dk4ng/a_little_treat_for_everyone/
function! ScratchIsFile()
    return expand('%') != ""
endfunction
function! ScratchIsSpecialBuffer()
    " if the buftype is one of the system ones, return 1
    let l:internalbufs = ["quickfix","help","directory","terminal"]
    if index(internalbufs, &buftype) >= 0
        return 1
    endif
    " if the filetype is one of the helper ones, return 1
    let l:helperbufs = ["filebeagle","netrw"]
    if index(helperbufs, &filetype) >= 0
        return 1
    endif
    " otherwise, return 0
    return 0
endfunction
function! ScratchSetBuftype()
    if ScratchIsSpecialBuffer()
        return
    endif
    if ScratchIsFile()
        set buftype=
    else
        set buftype=nofile
    endif
endfunction

augroup CreateMyScratch
    autocmd!
    " set Scratch for a new buffer
    autocmd BufEnter * call ScratchSetBuftype()
    " clear Scratch for a filled or a named buffer
    " (if some routine fills this buffer with a file (Netrw acts like this), or if we save it with a name)
    autocmd BufReadPost,BufFilePost * call ScratchSetBuftype()
augroup END


" FILETYPES

" Gitcommit
augroup OverrideFileTypeGitcommit
    autocmd!
    autocmd FileType gitcommit setlocal textwidth=0
augroup END


" TEST


